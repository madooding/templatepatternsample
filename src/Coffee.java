
public class Coffee extends CaffeineBeverage {

	@Override
	void brew() {
		System.out.println("Driping Coffee through filter");

	}

	@Override
	void addCondiments() {
		System.out.println("Adding some milk and sugar");
	}

}
